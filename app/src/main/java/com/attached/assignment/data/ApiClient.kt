package com.attached.assignment.data

import com.attached.assignment.data.responses.DataResponse
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface ApiClient {

    companion object {
        operator fun invoke(): ApiClient {
            return Retrofit.Builder()
                .baseUrl("https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/") // API Root path
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ApiClient::class.java)
        }
    }

    @GET("facts.json") // I was use GET method to retrieve the data
    fun getResponseData(): Call<DataResponse>

}
