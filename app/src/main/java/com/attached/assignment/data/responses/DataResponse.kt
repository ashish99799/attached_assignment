package com.attached.assignment.data.responses

data class DataResponse(
    var title: String? = null,
    var rows: List<RowData>? = null
)