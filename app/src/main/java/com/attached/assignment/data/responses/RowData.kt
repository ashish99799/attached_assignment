package com.attached.assignment.data.responses

data class RowData(
    var title: String? = null,
    var description: String? = null,
    var imageHref: String? = null
)