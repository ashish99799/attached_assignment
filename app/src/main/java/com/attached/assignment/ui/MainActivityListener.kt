package com.attached.assignment.ui

import com.attached.assignment.data.responses.DataResponse

interface MainActivityListener {
    fun onStarted()
    fun onSuccess(data: DataResponse)
    fun onFailure(message: String)
}