package com.attached.assignment.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.attached.assignment.R
import com.attached.assignment.data.responses.DataResponse
import com.attached.assignment.data.responses.RowData
import com.attached.assignment.utils.LoadImage
import com.attached.assignment.utils.ToastMessage
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main_cell.view.*

class MainActivity : AppCompatActivity(), MainActivityListener,
    SwipeRefreshLayout.OnRefreshListener {

    // Variable Declaration
    var context: Context? = null
    var viewModel: MainActivityViewModel? = null
    var rowsListAdapter: RowsListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Setup SupportActionBar to over Toolbar
        setSupportActionBar(toolbar)

        context = this

        // Init View
        initView()
    }

    fun initView() {
        // Attach ViewModel
        viewModel = ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
        viewModel!!.mainActivityListener = this

        // setup SwipeRefreshLayout
        swipeRefreshLayout.setOnRefreshListener(this)
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent)
        swipeRefreshLayout.isRefreshing = true
        onRefresh()
    }

    override fun onRefresh() {
        // Call Api
        viewModel!!.onRefreshData(context!!)
    }

    override fun onStarted() {
        // Api Calling started
        swipeRefreshLayout.isRefreshing = true
    }

    override fun onSuccess(data: DataResponse) {
        // Success API Response
        swipeRefreshLayout.isRefreshing = false
        supportActionBar?.setTitle(data.title)
        rowsListAdapter = RowsListAdapter(context!!, data.rows!!)
        listView.adapter = rowsListAdapter
    }

    override fun onFailure(message: String) {
        // Error & Failure
        swipeRefreshLayout.isRefreshing = false
        ToastMessage(message)
    }

    // ListView Adapter
    inner class RowsListAdapter
        (
        private val mContext: Context,
        private val items: List<RowData>
    ) :
        BaseAdapter() {

        override fun getCount(): Int {
            return items.size //returns total of items in the list
        }

        override fun getItem(position: Int): Any {
            return items[position] //returns list item at the specified position
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getView(
            position: Int,
            convertView: View?,
            parent: ViewGroup?
        ): View? {
            var convertView = convertView
            val viewHolder: ViewHolder
            if (convertView == null) {
                convertView = LayoutInflater.from(context)
                    .inflate(R.layout.activity_main_cell, parent, false)
                viewHolder = ViewHolder(convertView)
                convertView!!.tag = viewHolder
            } else {
                viewHolder = convertView.tag as ViewHolder
            }

            val currentItem = getItem(position) as RowData

            viewHolder.lblTitle.text = currentItem.title ?: ""
            viewHolder.lblDescription.text = currentItem.description ?: ""

            mContext.LoadImage(currentItem.imageHref ?: "", viewHolder.imgTopic)

            return convertView
        }

        inner class ViewHolder(view: View?) {
            var lblTitle = view!!.lblTitle
            var lblDescription = view!!.lblDescription
            var imgTopic = view!!.imgTopic
        }
    }
}
