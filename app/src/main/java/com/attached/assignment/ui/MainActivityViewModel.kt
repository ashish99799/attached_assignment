package com.attached.assignment.ui

import android.content.Context
import androidx.lifecycle.ViewModel
import com.attached.assignment.data.ApiClient
import com.attached.assignment.data.responses.DataResponse
import com.attached.assignment.utils.CheckInternetConnectionAvailable
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

// Override ViewModel
class MainActivityViewModel : ViewModel() {

    // Over Activity Listener
    var mainActivityListener: MainActivityListener? = null

    fun onRefreshData(context: Context) {
        // Check Internet connectivity
        if (context.CheckInternetConnectionAvailable()) {
            // API Calling Start
            mainActivityListener?.onStarted()

            // Ratrofit API Calling
            ApiClient().getResponseData().enqueue(object : Callback<DataResponse> {

                // Success Response
                override fun onResponse(
                    call: Call<DataResponse>,
                    response: Response<DataResponse>
                ) {
                    if (response != null) {
                        mainActivityListener?.onSuccess(response.body()!!)
                    } else {
                        mainActivityListener?.onFailure(response.message())
                    }
                }

                // Failure Response
                override fun onFailure(call: Call<DataResponse>, t: Throwable) {
                    mainActivityListener?.onFailure("Fail ${t.message}")
                }

            })
        } else {
            // Internet is not connected
            mainActivityListener?.onFailure("Please check your internet connection!")
        }
    }
}